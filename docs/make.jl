using Documenter, PictureOfTheDay

makedocs(format = :html,
         sitename = "PictureOfTheDay",
         pages = ["index.md"],
         repo = "https://gitlab.com/salvo981/PictureOfTheDay.jl/blob/{commit}{path}#{line}")
