"""
Test the retrieval of metadata for a given service
"""
function test_metadata_retrivial(pod::Pod, testroot::String, metadataPath::String)::NamedTuple{(:imgName, :dlUrl),Tuple{String, String}}
    @test fetch_metadata(pod, testroot) == metadataPath
    # test parsing
    imgInfo = parser(pod, metadataPath)
    @test isa(imgInfo, NamedTuple{(:imgName, :dlUrl),Tuple{String, String}})

    return imgInfo
end