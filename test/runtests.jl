using PictureOfTheDay
using Test

# Set global variables
testroot = joinpath(dirname(@__FILE__), "data")
infoDir = joinpath(testroot, "info")
imgDir = joinpath(testroot, "pictures")
tmpImgInfo = NamedTuple{(:imgName, :dlUrl)}(("", ""))

# metadata retrivial
@testset "Test NatGeo: Retrieve and download" begin
    include("podfetcher.jl")
    @testset "Metadata retrivieal and parsing" begin
        pod::Pod = Pod("natgeo")
        metadataPath::String = joinpath(infoDir, "$(pod.service).metadata.json")
        tmpImgInfo = test_metadata_retrivial(pod, infoDir, metadataPath)
        outimg = joinpath(imgDir, tmpImgInfo.imgName )
        # Check existance of dbfile
        imgDbPath = joinpath(infoDir, "downloaded.txt")
        @test isfile(imgDbPath)
        @test retrieve_img(outimg, tmpImgInfo.dlUrl, imgDbPath, false)
    end
end

# Image opening depending on the system
@testset "Image opening" begin
    println("Open the test JPEG image...")
    lenna = joinpath(imgDir, "Lenna_(test_image).jpeg")
    @test open_img(lenna, false) == 0
end