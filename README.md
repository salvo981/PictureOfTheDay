[![pipeline status](https://gitlab.com/salvo981/PictureOfTheDay/badges/master/pipeline.svg)](https://gitlab.com/salvo981/PictureOfTheDay/-/commits/master) [![coverage report](https://gitlab.com/salvo981/PictureOfTheDay/badges/master/coverage.svg)](https://gitlab.com/salvo981/PictureOfTheDay/-/commits/master)
# PictureOfTheDay

Just a simple program to retrieve the photo of the day (POD) from different web-pages.  

[Photo of the Day](https://www.nationalgeographic.com/photography/photo-of-the-day) from National Geographic is the only supported at present.  

This program was originally inspired by (the now defunct) [Pic-a-POD](https://picapod.com/), albeit no graphical interface has been implemented.  

`DISCLAIMER:` the program was started as a toy example to generate an installable JULIA package.

Potential future PODs that could be added:

- [Astronomy picture of the day](https://apod.nasa.gov/apod/)

- [Earth observatory](https://earthobservatory.nasa.gov/images/146635/tornado-leaves-a-mark-on-rural-texas)
- [Wikimedia POD](https://commons.wikimedia.org/wiki/Main_Page)  


## Usage
In order to download the picture the script `GetPod.jl` should be used.  
Using the default settings the program will download the picture from the National Geographic Picture of the Day web-page.
The main output directory must be specified.

Assuming that you downloaded the Package in your `home` directory, you can run the following command to download the image of the day.

```
myusername> julia ~/PictureOfTheDay/src/GetPod.jl -o ~/output_dir/
```

The above command will do the following: 
* Create a `info` directory (if not previously created) in which the main DB file (`downloaded.txt`) will be stored.
* Create a `pictures` directory (if not previously created) in which the downloaded images are stored.
* Update the file `downloaded.txt` prevent multiple downloads of the image.
* Visualize the downloaded picture using the system's default program to visualize image files.

To obtain a complete of the command line parameters run the following command

```
myusername> julia ~/PictureOfTheDay/src/GetPod.jl --help

PictureOfTheDay

Usage: GetPod.jl (-o <output_dir>) [-v] [-s <service>] [-d <db_file>]

Options:
    -o <output_dir>, --output-dir       Output directory [default: ]
    -d <db_file>, --db <db_file>        File with image list
    -s <service)>, --service <service>  Service to use [default: natgeo]
    -v, --verbose    Debug mode.
```

