"Package creation using PakgTemplates.jl"

using PkgTemplates

t = Template(;
            user="salvocos",
            host="gitlab.com",
            authors="Salvatore Cosentino",
            julia_version=v"1.2",
            plugins=[
                License(; name="GPL-3.0+"),
                GitLabCI(; coverage=true, extra_versions=["1.3", "1.4", "1.5"]),
                Documenter{GitLabCI}(),
            ],

    )