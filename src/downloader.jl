"""
Reads the file with the loaded images and compares it to the input image name
"""
function skip_download(toDownload::String, imgDbPath::String, debug::Bool=false)::Bool
    if debug
        println("\nskip_download@$(@__MODULE__) :: START")
        @show toDownload
        @show imgDbPath
    end

    if !(isfile(imgDbPath))
        write(Sys.stderr, "ERROR: the file with downloaded image list could not be found.")
        exit(-2)
    end

    # Search for image to download in the file with downloaded images
    outbool = false
    imgcnt::Int64 = 0
    for img in eachline(open(imgDbPath, "r"))
        imgcnt += 1
        if toDownload == img
            outbool = true
            break
        end
    end

    if debug
        @show imgcnt
    end
    return outbool
end



"""
Download the image and store it with the provided output name
"""
function download_img(dlUrl::String, imgPath::String, debug::Bool=false)::Bool
    if debug
        println("\ndownload_img@$(@__MODULE__) :: START")
        @show dlUrl
        @show imgPath
    end

    # Perform the image download
    download(dlUrl, imgPath)
    # check if the file was downloaded
    if isfile(imgPath)
        # add the downloaded image name into the db file
        return true
    else
        write(Sys.stderr, "WARNING: the image was not downloaded.")
        return false
    end
end



"""
Download the image and update img DB file
"""
function retrieve_img(imgPath::String, dlUrl::String, imgDbPath::String, debug::Bool=false)::Bool
    if debug
        println("\nretrieve_img@$(@__MODULE__) :: START")
        @show imgPath
        @show dlUrl
        @show imgDbPath
    end

    dlOk::Bool = false
    # Check that the img Path does not exist already
    if !skip_download(basename(imgPath), imgDbPath, debug)
        dlOk = download_img(dlUrl, imgPath, debug)
        if dlOk # add the entry in the DB
            ofd::IOStream = open(imgDbPath, "a")
            write(ofd, "$(basename(imgPath))\n")
            close(ofd)
        end
    else
        # If the image file is not in the directory, dowload it anyway
        println("INFO: picture found in DB.")
        println("Skipping download...")
        if !isfile(imgPath)
            println("\nWARNING: the image file\n$(imgPath)\nwas not found!")
            println("It will be downloaded again...")
            println("INFO: The DB file will not be modified.\n")
            dlOk = download_img(dlUrl, imgPath, debug)
        else
            return true # We set it to true although nothing was downloaded
        end
    end

    return dlOk
end
