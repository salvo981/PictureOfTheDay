using JSON: parsefile
using Dates
include("systools.jl")

"""
Retrieve and parse image metadata based on the service
"""
function fetch_metadata(pod::Pod, outdir::String)::String
    # Download the metadata
    localName::String = "$(pod.service).metadata."
    # set the name and extension based on the service
    if pod.service == "natgeo"
        localName = "$(localName)json"
    elseif pod.service == "second"
        localName = "$(localName)html"
    end
    # Retrieve metadata
    makedir(outdir, false)
    metaPath::String = joinpath(outdir, localName)
    download(pod.meta, metaPath)
    return metaPath
end



"""
Parses the metadata file
Returns the download link and final image name
"""
function parser(pod::Pod, metadataPath::String)::NamedTuple{(:imgName, :dlUrl),Tuple{String, String}}
    # Use a different function based on the service
    imgInfo = NamedTuple{(:imgName, :dlUrl)}(("", ""))
    imgName::String = dlLink = ""
    # set the name and extension based on the service
    if pod.service == "natgeo"
        imgInfo::NamedTuple{(:imgName, :dlUrl)} = parse_natgeo(metadataPath, pod.service)
    elseif pod.service == "second"
        imgName, dlLink = parse_second(metadataPath)
    end

    return imgInfo
end



"""
Parses metadata file from natgeo
"""
function parse_natgeo(jsonPath::String, srv::String)::NamedTuple{(:imgName, :dlUrl),Tuple{String, String}}
    imgLink::String = imgName = ""
    # parse JSON
    jsonDict = parsefile(jsonPath, dicttype=Dict, inttype=Int64, use_mmap=true)
    # Note that different informations for multiple days are listed depending on the key
    # 0 -> latest image, picture of the day
    # 1 -> yesterday
    # 2 -> day before yesterday
    # ...
    # 11 -> eleven days before
    # NOTE: a way to download multiple days could be implemented easily
    
    # For now the latest picture will be considered
    jsonDict = jsonDict["items"][1]
    # Start extracting the required information
    if haskey(jsonDict, "image")
        if haskey(jsonDict["image"], "uri")
            imgLink = jsonDict["image"]["uri"]
        end
    end
    # through an error if the link could not be found
    if length(imgLink) == 0
        write(Sys.stderr, "ERROR: the image link could not be found!")
        exit(-5)
    end
    # extract and parse the date
    imgdate::String = jsonDict["publishDate"]
    imgdate = date_parser(imgdate, srv)
    maintitle::String = jsonDict["image"]["title"]
    maintitle = title_formatter!(maintitle, srv)
    descr::String = jsonDict["social"]["og:description"]
    descr = title_formatter!(descr, srv)
    # create the final image name and path
    imgName = "$(imgdate)-$(maintitle)-$(descr).jpeg"
    return NamedTuple{(:imgName, :dlUrl)}((imgName, imgLink))
end



"""
Parses metadata file from second
"""
function parse_second(metadataPath)
    println("Parse second")
end



"""
Parses dates depending on the service
Returns a string with the date formatted as ddmmyyyy (e.g. May 11, 2020 -> 11052020)
"""
function date_parser(rawdate::String, service::String)::String
    outdate::String = ""
    if service == "natgeo"
        # example of raw date: May 11, 2020
        m, d, y = split(rawdate, " ", limit=3)
        # Convert month from literal to numeral value
        rawmonval::Int64 = Dates.monthname_to_value("May", Dates.LOCALES["english"])
        # add trailing 0 if required
        if rawmonval < 10
            m = "0$(string(rawmonval))"
        else
            m = string(rawmonval)
        end
        # format day of the month
        d = rstrip(d, [',', ' '])
        # add trailing 0 if required
        if parse(Int64, d) < 10
            d = "0$(d)"
        end
        # create the final date
        outdate = "$(d)$(m)$(y)"
    elseif service == "second"
        println("date:second")
    end
    # make sure the date is not empty
    if length(outdate) != 8 # it must be 8 characters (ddmmyyyy)
        write(stderr, "ERROR: the date ($(rawdate)) could not be properly parsed.")
        exit(-5)
    end
    return outdate
end



"""
Formats titles and description strings depending on the service
Modified the input string in place
"""
function title_formatter!(rawtitle::String, service::String)::String
    # Array of chars to be removed
    unwantedChars::Array{Char, 1} = ['\'', ' ', '.', ',']
    if service == "natgeo"
        # example of description:
        # Wedding in Italy in this National Geographic Photo of the Day.
        # remove service suffix "...in this National Geographic..."
        srvstr::String = " in this National Geographic Photo of the Day."
        if occursin(srvstr, rawtitle)
            rawtitle = rawtitle[1:end-length(srvstr)]
        end
        # suffix added around June 2020
        srvstr = " in this image from our photography archives."
        if occursin(srvstr, rawtitle)
            rawtitle = rawtitle[1:end-length(srvstr)]
        end
        # remove the unwanted chars
        for uc in unwantedChars
            rawtitle = replace(rawtitle, uc => '_')
        end
        # Remove special cases of unwanted characters
        rawtitle = replace(rawtitle, "__" => '_')
        rawtitle = replace(rawtitle, "_." => '_')

    elseif service == "second"
        println("date:second")
    end

    return rawtitle
end