"""Create a directory"""
function makedir(dpath::String, debug::Bool=false)::Nothing
    if debug
        println("\nmakedir@$(@__MODULE__) :: START")
        @show dpath
    end

    try
        mkdir(dpath, mode=0o751)
    catch e
        if !isdir(dirname(dpath))
            println("\nINFO: intermediate directories are missing and will be created.")
            mkpath(dpath, mode=0o751)
        else
            if debug
                println("\nINFO: The directory $(dpath)\nalready exists.")
            end
        end
    end

    return nothing
end



"""Visualize image file"""
function open_img(imgPath::String, debug::Bool=false)::Int
    # obtain system information
    islinux::Bool = Sys.islinux()
    if debug
        println("\nopen_img@$(@__MODULE__) :: START")
        @show imgPath
        @show islinux
    end

    # Run the display command in linux
    viewcmd::Cmd = `display $(imgPath)`
    if !islinux
        if Sys.isapple()
            viewcmd = `open -a Preview $(imgPath)`
        else
            write(stderr, "\nWARNING: this software can open the image only on Linux or Apple OS based systems\n")
        end
    end

    # Open the image
    procOut::Base.Process = run(pipeline(viewcmd), wait=true)
    # write in the log file
    if procOut.exitcode != 0
        println("ERROR: something went when trying to open the image file \n$(imgPath)")
        exit(-5)
    end

    return procOut.exitcode
end
