#=
Download image of the  day from different websites

COPYRIGHT: the copyrights that apply to the downloaded images depend on the web-service used. For more details visit the web-site of the service.
=#

# Command line interface
doc = """PictureOfTheDay

Usage: GetPod.jl (-o <output_dir>) [-v] [-s <service>] [-d <db_file>]

Options:
    -o <output_dir>, --output-dir       Output directory [default: ]
    -d <db_file>, --db <db_file>        File with image list
    -s <service)>, --service <service>  Service to use [default: natgeo]
    -v, --verbose    Debug mode.
"""

using Printf:@printf,@sprintf
using DocOpt

# add the path to the project directory to LOAD_PATH
scrPath = abspath(dirname(@__FILE__))
# @show scrPath
# add the path to LOAD_PATH
push!(LOAD_PATH, scrPath)
# @show LOAD_PATH

# Try loading the module
using PictureOfTheDay

#### Functions ####
"""
Create the required directories and DB files if not previously created
"""
function check_default_directories(outDir::String, debug::Bool=false)
    # Cretate pictures dir
    tmpPath::String = joinpath(outDir, "pictures")
    makedir(tmpPath, debug)
    # Cretate info dir
    tmpPath = joinpath(outDir, "info")
    makedir(tmpPath, debug)
    # Create the download files
    tmpPath = joinpath(tmpPath, "downloaded.txt")
    if !isfile(tmpPath)
        println("\nINFO: The file with the downloaded image list was not found.")
        println("A new one will be create in\n$(tmpPath)")
        touch(tmpPath)
    end
end

#####################

# obtain CLI arguments
args = docopt(doc)
@show args
outDir = abspath(args["--output-dir"])
srv = String(args["--service"])
debug = args["--verbose"]

# Expects the info and pictures directories to be inside the specified output directory
# This directory structure is used when no path for db file is specified
# NOTE: when using the default directory structure the metadata files
# will be stored in the same directory as the database file
defaultDirs = false

# Set the dbPath to default dir if no parameter has been given
if args["--db"] == nothing
    infoDir = joinpath(outDir, "info")
    dbPath = joinpath(infoDir, "downloaded.txt")
    defaultDirs = true
    println("INFO: using default directory structure.\n")
else
    dbPath = abspath(args["--db"])
    infoDir = outDir
end

# NOTE: in future a --use_default-dirs option could be added
# Set the image output directory
if defaultDirs
    imgDir = joinpath(outDir, "pictures")
    check_default_directories(outDir, debug)
else
    imgDir = outDir
end

if debug
    println("\nPOD settings:")
    println("Service: $(srv)")
    println("Use default directories: $(defaultDirs)")
    println("Output root directory: $(outDir)")
    println("Metadata directory: $(infoDir)")
    println("Database file: $(dbPath)")
    println("Image directory: $(imgDir)")
end

# create out if it does not exist
makedir(outDir, debug)

# load the service
pod = Pod(srv)

# # Download the metadata
metaInfoPath = fetch_metadata(pod, infoDir)
imgInfo = parser(pod, metaInfoPath)

if debug
    println("\nPOD retrievial summary:")
    println("Service URL: $(pod.url)")
    println("Image name: $(imgInfo.imgName)")
    println("Image URL: $(imgInfo.dlUrl)")
end

outimg = joinpath(imgDir, "$(imgInfo.imgName)" )

if debug
    println("Image path: $(outimg)")
end

retrieve_img(outimg, imgInfo.dlUrl, dbPath, debug)
open_img(outimg, debug)