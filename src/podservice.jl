"""
Represents a POD service.

fields: service, url, description.
"""
struct Pod
    service::String
    url::String
    description::String
    meta::String

    # constructor, make sure the values are positive
    function Pod(s::String)
        services::Array{String, 1} = ["natgeo", "second"]
        # make sure that the service name is valid
        if !(s in services)
            println("ERROR: the service $(s) is not valid, please choose one of the following:\n$(join(services, ", "))")
            exit(-5)
        else
            # set the url and description based on service name
            if s == "natgeo"
                new(s, "http://photography.nationalgeographic.com/photography/photo-of-the-day/", "National Geographic Picture of the Day.", "https://www.nationalgeographic.com/photography/photo-of-the-day/_jcr_content/.gallery.json")
            elseif s == "second"
                new(s, "http://set/a/url/", "Dummy description for service \"second\"", "http://set/a/url/metadata.html")
            end
        end
    end
end


"""
Overload summary and show functions for Point
"""
function Base.summary(io::IO, p::Pod)
    print(io, "POD service\nService -> $(p.service)\nURL -> $(p.url)\nDescription -> $(p.description)")
end

Base.show(io::IO, p::Pod) = summary(io, p)
