module PictureOfTheDay

include("podservice.jl")
include("podfetcher.jl")
include("downloader.jl")
include("systools.jl")



# expose the methods
export Pod,
    fetch_metadata,
    parser,
    retrieve_img,
    open_img,
    makedir

end # end module

